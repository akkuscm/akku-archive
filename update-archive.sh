#!/bin/bash
# SPDX-License-Identifier: GPL-3.0-or-later
set -ex
. config

(cd spegel;.akku/env scheme --program bin/spegel-timestamps ../archive/packages/ > ../archive/timestamps.scm)

akku archive-scan archive/packages/ archive/packages/snow/ > archive/Akku-origin.scm
akku archive-scan archive/pkg/* archive/packages/snow/ > archive/Akku-index.scm

xz -v9 < archive/Akku-origin.scm > archive/Akku-origin.scm.xz
xz -v9 < archive/Akku-index.scm > archive/Akku-index.scm.xz
rm -f archive/Akku-index.scm.sig
rm -f archive/Akku-index.scm.xz.sig
rm -f archive/Akku-origin.scm.xz.sig
gpg --digest-algo SHA256 -bs -u "$GPG_KEY" archive/Akku-index.scm
gpg --digest-algo SHA256 -bs -u "$GPG_KEY" archive/Akku-index.scm.xz
gpg --digest-algo SHA256 -bs -u "$GPG_KEY" archive/Akku-origin.scm.xz

rm -rf archive/mirror
cp -ldR mirror/ archive/

pushd spegel
.akku/env bin/spegel-metadata
popd
rm -f archive/metadata.scm.xz
xz -v9 archive/metadata.scm

# Access by sha256
SHA256=$(sha256sum archive/Akku-index.scm | awk '{print $1}')
DIRNAME=$(echo "$SHA256" | dd bs=2 count=1 2>/dev/null)
rm -rf archive/by-sha256
mkdir -p "archive/by-sha256/$DIRNAME"
cp -f archive/Akku-index.scm "archive/by-sha256/$DIRNAME/$SHA256"
cp -f archive/Akku-index.scm.sig "archive/by-sha256/$DIRNAME/$SHA256.sig"

# Access by sha1
SHA1=$(sha1sum archive/Akku-index.scm | awk '{print $1}')
DIRNAME=$(echo "$SHA1" | dd bs=2 count=1 2>/dev/null)
rm -rf archive/by-sha1
mkdir -p "archive/by-sha1/$DIRNAME"
cp -f archive/Akku-index.scm "archive/by-sha1/$DIRNAME/$SHA1"
cp -f archive/Akku-index.scm.sig "archive/by-sha1/$DIRNAME/$SHA1.sig"

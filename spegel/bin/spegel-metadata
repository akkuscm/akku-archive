#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2019, 2020 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: GPL-3.0-or-later

(import
  (rnrs (6))
  (akku lib manifest)
  (only (akku lib utils) path-join
        url-join mkdir/recursive sanitized-name)
  (akku lib git)
  (akku lib repo-scanner)
  (akku private http)
  (chibi match)
  (only (spells filesys) file-directory?)
  (only (srfi :1 lists) last filter-map)
  (only (srfi :13 strings) string-suffix? string-contains string-prefix?)
  (only (srfi :67 compare-procedures) <? default-compare)
  (wak fmt)
  (xitomatl alists)

  (only (akku private compat) putenv)
  (only (akku lib utils) run-command)
  (akku lib file-parser)

  (only (xitomatl common) pretty-print))

(define (index-filename)
  "../archive/Akku-index.scm")

(define (snow-mirror-directory)
  "../mirror/snow")

(define (repack-mirror-directory)
  "../archive/pkg")

(define (git-mirror-directory)
  "../mirror/git")

(define (url->file-location url)
  (cond ((string-prefix? "http://snow-fort.org" url)
         (path-join (snow-mirror-directory)
                    (substring url (string-length "http://") (string-length url))))
        ((string-prefix? "https://archive.akkuscm.org/archive/pkg/" url)
         (path-join (repack-mirror-directory)
                    (substring url (string-length "https://archive.akkuscm.org/archive/pkg/")
                               (string-length url))))
        (else
         (error 'url->file-location "What?" url))))

(define (read-package-index index-filename) ;XXX: should be in akku
  ;; Read packages from the index.
  (call-with-input-file index-filename
    (lambda (p)
      (let lp ((pkg* '()))
        (match (read p)
          ((? eof-object?) pkg*)
          (('package ('name name)
                     ('versions version* ...))
           (lp (cons (make-package name (map parse-version version*)) pkg*)))
          (else (lp pkg*)))))))         ;allow for future expansion

(define (canonicalize-git-url url)
  (cond
    ((string-suffix? "/" url)
     (canonicalize-git-url (substring url 0 (- (string-length url) 1))))
    ((and (not (string-suffix? ".git" url))
          (or (string-contains "github.com" url)
              (string-contains "gitlab.com" url)))
     (canonicalize-git-url (string-append url ".git")))
    (else
     url)))

(define (git-url->local-path url)
  (substring url (+ 3 (string-contains url "://")) (string-length url)))

(define (gather-metadata pkg ver git-dir p)
  (let ((artifact-data
         (filter-map
          (lambda (a)
            (let ((general
                   `((form ,(artifact-form-index a))
                     (usage
                      ,(filter-map (lambda (proc flag)
                                     (and (proc a) flag))
                                   (list artifact-for-test? artifact-for-bin? artifact-internal?)
                                   (list 'test 'bin 'internal)))
                     (implementation ,(artifact-implementation a))
                     (imports ,@(map (lambda (lib-ref)
                                       (list (library-reference-name lib-ref)
                                             (library-reference-version-reference lib-ref)))
                                     (artifact-imports a)))
                     (assets ,@(map (lambda (file-inc)
                                      `((path ,(include-reference-path file-inc))
                                        (conversion ,(include-reference-conversion file-inc))))
                                    (artifact-assets a))))))
              (cond
                ((r6rs-library? a)
                 `(r6rs-library (name ,(r6rs-library-name a))
                                (version ,(r6rs-library-version a))
                                (exports ,@(r6rs-library-exports a))
                                ,@general))
                ((r7rs-library? a)
                 `(r7rs-library (name ,(r7rs-library-name a))
                                (exports ,@(r7rs-library-exports a))
                                ,@general))
                ((module? a) `(module ,(module-name a)
                                ,@general))
                (else #f))))
          (find-artifacts git-dir (scm-file-list git-dir)))))

    (pretty-print `(package-version (name ,(package-name pkg))
                                    (version ,(version-number ver))
                                    (lock ,(version-lock ver))
                                    (license ,(version-license ver))
                                    (artifacts ,@artifact-data))
                  p)
    (newline p)))

(define (shine-mirror pkg metadata-port)
  (for-each
   (lambda (ver)
     (let ((lock (version-lock ver)))
       (match (assq-ref lock 'location)
         #;
         [(('git (? string? url)))
          (let* ((url (canonicalize-git-url url))
                 (path (git-url->local-path url))
                 (revision (car (assq-ref lock 'revision)))
                 (tag (cond ((assq-ref lock 'tag #f) => car) (else #f))))
            (fmt #t nl "Checking " (wrt (canonicalize-git-url url)) " in " (wrt path) nl)
            (let ((git-dir (path-join (git-mirror-directory) path)))
              (let ((head (git-rev-parse git-dir "origin/HEAD")))
                (git-checkout-commit git-dir revision)
                (when tag
                  (let ((tag-rev (git-rev-parse git-dir "HEAD")))
                    (unless (equal? tag-rev revision)
                      (fmt (current-error-port) "BAD REVISION! Tag: " tag ", tag-rev: " tag-rev
                           ", but revision: " revision
                           nl))))
                (gather-metadata pkg ver git-dir metadata-port)
                (when (eq? ver (last (package-version* pkg)))
                  (unless (equal? head revision)
                    (fmt #t "HEAD DIFFERS FROM LATEST!" nl
                         "Latest packaged version: " revision
                         " version " (version-number ver) nl
                         "HEAD: " head nl))))))]
         [(('url (? string? url)))
          (fmt #t "Tarball " (wrt url) nl)
          (let ((local-path (url->file-location url)))
            (fmt #t (wrt local-path) nl)
            (putenv "AKKU_TARBALL" local-path)
            (run-command "set -e; rm -rf /tmp/akkutemp; mkdir -p /tmp/akkutemp; tar --warning=no-timestamp -C /tmp/akkutemp -xaf \"$AKKU_TARBALL\" || true")
            (gather-metadata pkg ver "/tmp/akkutemp" metadata-port))
          #f]
         [else #f])))
   (package-version* pkg)))

(let ((index (read-package-index (index-filename))))
  (call-with-port (open-file-output-port "../archive/metadata.scm" (file-options no-fail)
                                         (buffer-mode block)
                                         (native-transcoder))
    (lambda (p)
      (fmt p ";; This is file-parser output for all packages at akkuscm.org" nl)
      (for-each (lambda (pkg)
                  (shine-mirror pkg p))
                (list-sort (lambda (x y) (<? default-compare
                                             (package-name x)
                                             (package-name y)))
                           index)))))

#!/bin/bash
# Copyright (C) 2018, 2019, 2021 Göran Weinholt <goran@weinholt.se>
# SPDX-License-Identifier: MIT

# Akku.scm installation script meant for CI systems that do not use
# Docker.

# Example usage:

# gpg --keyserver ha.pool.sks-keyservers.net --recv-keys E33E61A2E9B8C3A2
# curl -LO "https://archive.akkuscm.org/get-akku.sh{,.sig}"
# gpg --verify get-akku.sh.sig && bash get-akku.sh

#   -- or --

# gpg --import akku-archive-2018.gpg
# curl -LO "https://archive.akkuscm.org/get-akku.sh{,.asc}"
# gpg --verify get-akku.sh.asc && bash get-akku.sh

set -e

VERSION=1.1.0

UNAME=$(uname)
MACHINE=$(uname -m)

case "$UNAME-$MACHINE" in
    Linux-x86_64)
        FILENAME=akku-$VERSION.amd64-linux.tar.xz
        ;;
    # Linux-arm*|Linux-aarch64)
    #     FILENAME=akku-$VERSION+arm-linux.tar.xz
    #     ;;
    *)
        echo "Akku.scm is currently not pre-built for $UNAME $MACHINE."
        FILENAME=akku-$VERSION.src.tar.xz
        ;;
esac

case "$UNAME" in
    Darwin)
        TAR=gtar
        ;;
    *)
        TAR=tar
        ;;
esac

# pub   rsa4096/0xE33E61A2E9B8C3A2 2010-10-12 [SC]
#       08272FBB54EEB5072D5BA930E33E61A2E9B8C3A2
# uid                   [ultimate] Göran Weinholt <goran@weinholt.se>
# uid                   [ultimate] Göran Weinholt <weinholt@debian.org>
# sub   rsa4096/0x374EFA1F39CB3BD1 2010-10-12 [E]
# sub   rsa4096/0xF77DF07F215EC273 2017-09-16 [S] [expires: 2022-09-15]
gpg --import <<EOF
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBEy0qsYBEACwRFGK0U84RtGCdURzT/CUGD+XxZNGVU/o6f74sHywmnPAjgWD
AWZEvHUGMYtaN7vMPE77aKSLHEYXhvO9QJrcT2QAbG3WoaolOpnxYv2QgwBTTGTy
Lu+ngDlpZqcFRnhmGyhEEf9t1CbuQ7guvJcsJ5xsSP0LrgXbzmwoQW1C569d1cAD
1of8Lln4Zwy0FmX6jb5oYy4p25xecvM3BFla64xMiMwU1gAi7RN1WIWqAyvSxNVm
xpaWeWTrjoMFxM2K7Hh7UnBgEC/VzTicSK5YVeak7CbmwEG2Oi/ocVdB6AmWu/s1
RM3fIkIOWnydR9pss9TYO6cUaDCUmxj6gHlsL/21Sy7SY3DoCiKER8L2zww/uMpS
o1y7Y28avjwaPHYnJrOYhQBaUhRO2Ge9XpYKeh5puS1UPrt5NmMwob/UZPpOC8q0
mCwCxZvKfR2sprY/KiOyErYpwH8TP73ALpHwvDHfq+hB+vv13UPkNCSfMCmaXt90
GYI1U8Fx4yHDPttTYmJYc0pteC0vc2a8lUwpoqbBHg1STUZWIV9zLKCRiDdB89hG
iMfCnidJ7dyBzbnv5KDdT99CP9eSaLA/LiOeChIRgk3K/xTLVH+9Mi/pFZXG+Tp1
ehyncoNZu5PkTmifYCDCnYYeY5U/WlgVNkQjsrUkc4KIJbJPBLEb0/lLpwARAQAB
tCVHw7ZyYW4gV2VpbmhvbHQgPHdlaW5ob2x0QGRlYmlhbi5vcmc+iQJOBBMBCgA4
FiEECCcvu1TutQctW6kw4z5houm4w6IFAlr0DQoCGwMFCwkIBwMFFQoJCAsFFgID
AQACHgECF4AACgkQ4z5houm4w6Kp0BAAhSPK2n2dbXIdLfFFTTzNvZdswzqydE66
frtkessE0FhTCvrLcvKZ9C5IJlBBOLZ8rw3jeLpKGkd46EiJU52x0gFqPmQD7c31
wXDoSS3CG5Daz1ohWAa24DDniPwVuaw3UPxFdWkgqxgSdIo52oqBBCCqm71j1EG+
WjjU2nPd7G0pKMdM4o44Lnt/VthuGL8fbCs5RBQ/Vrtoe+zveLAuWEQPV8IsECIP
08IsH5IH5r/u2UU7J25YNxgkDTYGC2ZsvcPZvkuK16Ps7XbVr1sz7pw98kxMCen9
+UvfrLlDyL7byQdkWLz4sKzu/vGVmc9wR4zmEVNqHstotRfnesfwOy80O+FV30Y1
FEPoR3plJZRPzCzOY7yyTAM9GwtS14m8BjnDV7JITPRByF+ONaYNVizI6sho+7GY
EdrXJu5FOIzbeMupIp7unoKITjkE57Ruct9R5nNCrW6JVEhgKw0i6GSt9UHptVJt
KO86dRZS+tvgBM6avoAw3XW95zuRRIMLz4Qsult4RNGXDFM7WP/KI2RKDh7gz4jE
7T8jbQy+WUQGG+g+MXmsVSWpWWyHHmTfGwggHB5t2dtYQ3ovgi6UTdWtDa91nHKg
6AR1RkWb7Qd5sfhbr0CITaHJZAONPfyA7axCbpHF6BFzBlC5NFtInI9IuJDXwJ27
p4c3sRX3o8G0I0fDtnJhbiBXZWluaG9sdCA8Z29yYW5Ad2VpbmhvbHQuc2U+iQJP
BBMBCgA5AhsDAh4BAheABAsJCAcGFQIICQoLAxYCABYhBAgnL7tU7rUHLVupMOM+
YaLpuMOiBQJa9A1QAhkBAAoJEOM+YaLpuMOikqoQAIF5kp6cwOeeTce/dp2AHLeI
FpgrNoHmBZPuYByv+ISwtjVZK3hNfoGmTrzRgnb6I5SN1JwY421uK6bkz3+GmD5t
1b8TibTazvsj6DOJz1hZrDVdmMNrcIOjxtIwcfo+bceC2y6pL/yB/pjJDAgWGEkb
OQQ4av6h8D2IZDn+MuH9WhlrLQRg8DQIJxR8Pf4J8DEngP9A3F3i8SGHI0TCAYfE
gX6vjGf34rcp9vBK2WYuaOIxne1ZV3BXQ91UdApmqoZsHDVQB4QpYjYJ0m9IkRPp
54HF7e9WGOJqKkVJssrVycJC4QqjuuaagxWy3YAzA64W99VLwACJB1PJrLz2lG/J
1zTBeswmk7ENlAJA/ka/1vjf3gJEpgS4XjnZxvIfFLuC5+ffqeVibBRyb9n4NEw7
yNbitn9RYdp3sZvW+rcppaUhrTTQZPr5J8t6Ne5hpQzK6G9CeJU+eT5/W7G1wEnR
bIryUeAfTzhH8pczppYQiEl17LMBOXVRd305C31u8Iyim3F8awJ1Gi5IUiiT4XCL
JZpEzvcSvltFtJ8G9vohZt3FRbHXOqSepxmyqqNK55gSnaQLMk8yBYbv1OU8iElP
xHGocB7NFGYvEspctY8/34AJP4hh6bk5nvXYLXTJqQVG2vn/ruZinTnGdYMB6Gug
J1lefJNfmdVuuPy/fKNhuQINBEy0q9YBEADA03MZqAw1aX/REVcFq61CbskAckiu
kN6vvGipPmFxYktVvdMDxaTfq1Ls4Hz3DEWoFObOr+ANt5YtOWkbMtZPMoGryo1e
PCIe+1CjoK3+U82+Vy2upYLp6piFDviUaabThFlwHmTUJI4stbXa8QWEIkJT+M+m
VDO9DfYVfiF+Riy1YgFxLsN0u6srQfEYTeWCss3gy3RRIqXNzrqhWywoGd9hwUgT
PFSYsPcpliOmw9TnIJTP73wnwBufMck8hdbPa70FcAekHICaIkmNfo5Os8/xr8cW
DS3XAItbtq/f2WvP0pnALbc7u672u3b9+gCNi1LD4vZBfETesmTDfy5/SsxeWxCe
3vdJ2uo7n9Bo89TGnbkBzvK16+8p+SWt8BoVIsN3xJd+RhcATOnnx8ekmvZ7SHvD
c/WMaHGlVw7S+GVuYXPIkVcUeHTXX7OE6spZl5dBlHVP3eFTXslowG0ftpUy25jl
t6mQ+0nq44inq6Qm433E9DYLUfiCblMLAxAyNiyvuCfMea2oLYiRPIRncb5C07mN
j9P4HHcl0d+F/HZnEpWUWmw5o0suYpmYr03311jQ5TFaJjh0FEuZwUHZW/nGvtaH
ZBAq+DEDnTFaXGhp3ACQ2RgOO6XBoNNNdpdWZxWEN9jSCfOCeO2RdgDsAsRMlLqB
ih8ecJ0mQ38LDwARAQABiQI2BBgBCAAgAhsMFiEECCcvu1TutQctW6kw4z5houm4
w6IFAlm9GmYACgkQ4z5houm4w6KBjg//dv/AYIVaaqy8fgyzkQO+DQsHd6Z9XnlH
BZMRRduCkI0d7OsSOiKrDQD/vaTaOV1V+WXXfJxNzd9/+OPQtdD10tQzZw5xglZR
ynjD0LZu78kY3wS6CSaZPY5ddaIBVRsfvu6EKQaNNs8xaXEzmtI34Rm/Xz8t/eeC
rClZ8hCaA5X5CkXFq+QzLo2uIbMvwkPfCzpGIhjo4m8nkw58UoBa5LGxEGg8EkbL
1DEMBCPPS6bdYdjXsMM4SQ7WdETW+M48I5aS2ubiMrdoa7qKXNnU6VfapBwmsf3b
CwS5UL+cKEz0hmVMhbVyV+yEHzFbTRBKl5mpqbQTVFtrOcU7F3YPvFVlYib5KBBr
axzeXV5ST4A3gq5Yss3IJ3YJH3pk5nMJ/3iM/bDoTmLvrcGCenogX9y1X6wfSn+Y
k3iH0YrBY0hVbw1bb4KglvHG0lm1rhMpv3bRiCMiWpkmA3Fyf0txxP40wvfYw00/
d3/b344yQhhzG5VXuoq2nxwtSnH5O6mFv1mjLTIChVPs5TWAdZHkh6nhXOQzfO7L
dMSjhBwMm7X0DWbVN4QnleiKorDXQAvs3xzIhsfD6a2WXT8t0wDud+hLBLnRby9o
r+KGj/cfIc6KWbT7JstUNxYrz+nNyBmkEcMEqPxwcfBY/x/TrOlCmiRuPmzKKmHx
ekedl2xreum5Ag0EWb0afQEQALjFmD5CbyNFph/HEIcehsutK9wrhSKUQ3Fr67fP
fN8DVhR7MYtXQqdnswSv7vTa/Djm0ULx9Bk5zVhWIE/z4wA1xnzbhjle8PPPthtw
c4192MFUFqvckzMN5/glovhqXzSQa3CM701Ebtbgi7nLeTuUW5PvVRkzgk8OZvIr
W1XRTJFFUWZZ5rWn4j3InwGoaLKU9lOw/+SjtqeMDELHoQIkOqApQbxRnk9Z0zmV
Lt7RCYdvCQiL75CNsSIlsMe/9zP3RV2xz4AmuMopOx+Hu339PoqfiHuoXDcAxHq8
pO3NOXFxAn9V3uc/EcyPMfRpNzkTrCDTiyWfLZBb2aGyyMQOAp6a+orl8Z4dtP9B
T2xmEDupM2UCQ5/C6q6uyKDmHW1vdecb/VTPGafO+AXvL3ohWrRJowGbCmn+zDBi
aLyIaIeotDrVXDdXtyn2lMipKwr0GYUIj+mdwij5Hfu2RFp4ozEBMxwCm3jqVoiE
/4hvXDiwAICYtNPTNEnbqjMYV4fCyzpKdlUH76W/EOyQxNZ6GW8XgKpE2KDT3iBz
r+BKRrr1z+87g13PrdTL/oM4l/lvIiPFmwSiJLcDkCOcQi5R2MkNkuYmPJP8oUug
wedZnJTzGVvT5eAbCsh57PQBA+W+UCNmgQRMK1fhXbJg75YM9wvwCK5w3KvFk2y+
rQdVABEBAAGJBHIEGAEIACYWIQQIJy+7VO61By1bqTDjPmGi6bjDogUCWb0afQIb
AgUJCWYBgAJACRDjPmGi6bjDosF0IAQZAQgAHRYhBKvvUlmCBaIw81lAcPd98H8h
XsJzBQJZvRp9AAoJEPd98H8hXsJzJt4QAIRu34Z/U4vIEGFz904AuSyt4uv82c71
caV8Xjc5huFk/QAtQKbg7cirIg+MfLmk1I2wahvMFK1efFmD3m1vPFWKMlq0ADVQ
+uAPySEHaZ4bXqqz4qy2OePE3ILHA7KoANsPbo6wmbzICqPfgbjnO4kv2tau5FDA
LivF0Q84Afnv5IWWIiiVBxzndxddIQ6/j6qrH67hefQYqLbh/xlLWr5DSsJ5dSDl
vfw2IEIty110AMOYJ0ETYwqkTjHDjfHePV8KYUXubrOpmJdT7vZ//ZoprDbB1rtP
DDD8goozK8HQ/F3PTduZ6ys3HAMot2FEV5WzfdSmlk0QER+fM2Yy45mVej0646om
BZIGUhLyURe7b56jJc9vLa1whVxREGzCTcwmnIOSVNL4yMwyXGOj8wUDPGsfUe25
OLuNZcwS+796yriF2TYkIuXDfPBHj9mAu3H4Ob/9mYzH9Oj+ZFGJ3Xu76/F4latG
RWyFC7BtKzNMGEnKlw3QdotZzjCHIl31TOMHlzkbdTmMJ8Fmb8LRj9Yo4QDqrq5m
zGGPPzNEd5ktr+ehTz3I9XuvxZV11QNIuGpx3DEZW3RjNLArvUaIaJlc6Tp4Q/ff
5SZ9WqVx9kKWxuIestlN2DV6YfaVfMAyfolU0lG4cs9dOx5rWRM9TDsjyB3kNfQk
q77U+2NZY4zYq/oQAKDvZxkPc4hRvTmm5NMi6ObZWuTqTOOG9vupAxBNWfUznTnj
mHTwLxK0nTTtzHN7+qdhviFHAMV6gqRY6F2s1dGLAFg7QXpMthGuVxkfBA2BtP0K
A3gifFBEoLg+hqNA0gyHEp+upZcIU4Uq4bhOLurRYvOaw/QpHLA/dAfzCATyvsZQ
v2unWXP9egVDlk6i779FU4KQ6eNWo0Izi3+pxSZFQRnU93a9Dur+cElNjeN072Z+
agd+rrHTTdKQfwGjQFMWFlhaSYoOp8N5QqamEO2EB9a3X2i61X7uHbiHSwRA5fjY
UEs93MfwCZc8gvylkEISaxxPCbSIInTFAaO0ZMKA7KSXCuBB3dGxxcs5ePtCIp1+
jYLiGZMZv7D6wSXIPsmrfMkJjnKcNPptBGkMRXUim1pepRi6hmMDlrEqsZdcslag
2lHttAQW+o3nEaptjDorPD0NE4PqXUAdAfa7zmJ4UOiw46eFSyEsnDP+cjiqKGWL
nool6hfNhG+CoNqAqh/s/gyXkNzPbjmuhmw5SQGpEhf/1Wak/Bt81hnCn7jLVCZ8
29QvtcVSWNUOgfQO3dtbg/yB9ZpMvH5ytadG1Sko4FTSQT5hanaqTDj1SsRKWgwP
bFANUM8AoZXloDwEunpiu0L2gIQwbCY3s8A+xA9mG1hDPNY/p1tofRXSW3gm
=4P+X
-----END PGP PUBLIC KEY BLOCK-----
EOF

curl --remote-name-all -L https://github.com/weinholt/akku/releases/download/v$VERSION/$FILENAME{.sig,}
gpg --verify $FILENAME.sig $FILENAME || exit 1

mkdir -p akku-install
$TAR -xvJf $FILENAME -C akku-install --strip-components 1
cd akku-install
./install.sh
# For Akku 1.0.0 to be backwards compatible with old CI setups:
mkdir -p ~/bin; ln -s ~/.local/bin/akku ~/bin/akku
exit 0

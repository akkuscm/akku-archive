#!/bin/bash
# SPDX-License-Identifier: GPL-3.0-or-later

set -ex

. config

# Copy all referenced files first.
for dest in $SERVERS; do
    rsync -av archive/by-sha256 "$dest"
    rsync -av archive/by-sha1 "$dest"
done

# Then update all files, including files doing the referencing.
for dest in $SERVERS; do
    rsync -av archive "$dest"
done

sleep 10s

# Then delete out of date files.
for dest in $SERVERS; do
    rsync --delete-after -av archive "$dest"
done

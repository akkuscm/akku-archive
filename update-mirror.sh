#!/bin/bash
# SPDX-License-Identifier: GPL-3.0-or-later
set -e

pushd spegel
mv ../mirror/snow/snow-fort.org/s/repo.scm repo.scm.old
# XXX: spegel-snow can make new packages appear. You are prompted to
# review every package.
.akku/env bin/spegel-snow

# This creates a git mirror of every submitted package. It can be used
# in case the original repository goes away.
.akku/env bin/spegel-git

# Repack packages in git as tarballs.
.akku/env bin/spegel-mktar

# XXX: Run update-archive.sh again after this.

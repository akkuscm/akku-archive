#!/bin/bash
# SPDX-License-Identifier: GPL-3.0-or-later

set -ex
cd ../akku-web/
bash rebuild.sh

#!/bin/bash
# SPDX-License-Identifier: GPL-3.0-or-later

. config

gpg -bs -u E33E61A2E9B8C3A2 get-akku.sh
gpg -a -bs -u C2840E6C4169DB68 get-akku.sh

for dest in $SERVERS; do
    scp get-akku.sh{,.sig,.asc} $dest
done

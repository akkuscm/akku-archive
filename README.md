# Akku.scm archive software

This is the software that is used to manage https://archive.akkuscm.org/.
It is only needed if you want to run your own Akku archive.

## Configuration

Create the `.config` file with the following contents:

```
SERVERS="server1:/srv/archive.akkuscm.org/ server2:/srv/archive.akkuscm.org/"
GPG_KEY="long-key-id"
```

The servers are scp/rsync destinations. The GPG key is used to sign
the Akku-index.scm file. Clients pointed at the repository must have a
copy of its public key.

## Source material

The primary source material for the archive software is the
`archive/packages` directory. Place `.akku` and `.akku.sig` files here
to make them part of the archive. The signatures are only checked when
files are moved into this directory. For now this is a manual step and
verification of the referenced package contents is likewise manual.

The `archive/packages/snow` directory is automatically created by
`spegel-snow`. It downloads the repository index from snow-fort.org,
downloads and verifies every archive, and opens every file in a pager
for manual verification.
